OCP_WILDCARD_DOMAIN=54.172.209.51.nip.io

oc create route edge ldap-demo-stage --service=apicast-staging --hostname=ldap-demo-apicast-stage.${OCP_WILDCARD_DOMAIN} -n 3scale
oc create route edge ldap-demo-prod --service=apicast-production --hostname=ldap-demo-apicast-prod.${OCP_WILDCARD_DOMAIN} -n 3scale
