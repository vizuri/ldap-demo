package com.vizuri.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
@EnableAutoConfiguration
@SpringBootApplication
public class LdapDemoApplication {
	
	private static Logger logger = LoggerFactory.getLogger(LdapDemoApplication.class);


	public static void main(String[] args) {
		logger.info(">>>> STARTING <<<<<<");
		SpringApplication.run(LdapDemoApplication.class, args);
		
		logger.info(">>>> FINISHED <<<<<<");
	}

	
	
	


}