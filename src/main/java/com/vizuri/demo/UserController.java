/*
 * Copyright 2015 Vizuri, a business division of AEM Corporation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.vizuri.demo;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.annotation.Resource;
import javax.naming.Name;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.SearchControls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DistinguishedName;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = { "http://localhost:3000", "*" })
@RestController
@RequestMapping("/user")
public class UserController {

	private static Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private LdapContextSource context;

	@Autowired
	private LdapTemplate ldapTemplate;

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public Iterable<?> findAll() {
		logger.info(">>>>>> Find All Users");
		SearchControls controls = new SearchControls();
		controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		
	
		
		List<User> list = ldapTemplate.search(DistinguishedName.EMPTY_PATH, "(objectclass=person)", controls,
				new UserAttributesMapper());
		
		logger.info("Found Users:" + list.size());
		return list;
	}

	@RequestMapping(method = RequestMethod.GET, produces = "application/json", value = "/{id}")
	public User findById(@PathVariable("id") String id) {
		logger.info(">>>>>> Find User:" + id);
		return null;
	}

	@RequestMapping(method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public User create(@RequestBody User user) {
		logger.info(">>>>> Creating User:" + user.getUsername() + ":" + user.getPassword());
		logger.info("context:" + context);
		logger.info("ldapTemplate:" + ldapTemplate);

//	    Name dn = LdapNameBuilder
//	      .newInstance()
//	      .add("ou", "users")
//	      .add("cn", user.getUsername())
//	      .build();
//	    DirContextAdapter context = new DirContextAdapter(dn);
//	 
//	    context.setAttributeValues(
//	      "objectclass", 
//	      new String[] 
//	        { "top", 
//	          "person", 
//	          "organizationalPerson", 
//	          "inetOrgPerson" });
//	    context.setAttributeValue("cn", user.getUsername());
//	    context.setAttributeValue("sn", user.getUsername());
//	    context.setAttributeValue
//	      ("userPassword",user.getPassword());
//	 
//	    ldapTemplate.bind(context);

		Attribute objectClass = new BasicAttribute("objectClass");
		{
			objectClass.add("top");
			objectClass.add("uidObject");
			objectClass.add("person");
			objectClass.add("organizationalPerson");
		}
		Attributes userAttributes = new BasicAttributes();
		userAttributes.put(objectClass);
		userAttributes.put("cn", user.getFirstName());
		userAttributes.put("sn", user.getLastName());
		userAttributes.put("uid", user.getUsername());
		// userAttributes.put("postalAddress", user.getPostalAddress());
		// userAttributes.put("telephoneNumber", user.getTelephoneNumber());
		userAttributes.put("userPassword", user.getPassword().getBytes());
		ldapTemplate.bind(bindDN(user.getUsername()), null, userAttributes);
		
		

		return user;
	}

	public static javax.naming.Name bindDN(String _x) {
		@SuppressWarnings("deprecation")
		javax.naming.Name name = new DistinguishedName("uid=" + _x + ",ou=People");
		return name;
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	public User update(@PathVariable("id") String id, @RequestBody User user) {

		return null;
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public void delete(@PathVariable("id") String id) {
		logger.info(">>>>> Deleting User:" + id);
	}

	private class UserAttributesMapper implements AttributesMapper<User> {

		@Override
		public User mapFromAttributes(Attributes attributes) throws NamingException {
			User user;
			if (attributes == null) {
				return null;
			}
			user = new User();
			user.setFirstName(attributes.get("cn").get().toString());

			if (attributes.get("userPassword") != null) {
				String userPassword = null;
				try {
					userPassword = new String((byte[]) attributes.get("userPassword").get(), "UTF-8");
				} catch (UnsupportedEncodingException e) {
					logger.error("unable to process", e);
				}
				user.setPassword(userPassword);
			}
			if (attributes.get("uid") != null) {
				user.setUsername(attributes.get("uid").get().toString());
			}
			if (attributes.get("sn") != null) {
				user.setLastName(attributes.get("sn").get().toString());
			}
			// if (attributes.get("postalAddress") != null) {
			// user.setPostalAddress(attributes.get("postalAddress").get().toString());
			// }
			// if (attributes.get("telephoneNumber") != null) {
			// user.setTelephoneNumber(attributes.get("telephoneNumber").get().toString());
			// }
			return user;
		}
	}
}
