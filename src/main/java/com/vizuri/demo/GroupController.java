package com.vizuri.demo;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = { "http://localhost:3000", "*" })
@RestController
@RequestMapping("/group")
public class GroupController {
	private static Logger logger = LoggerFactory.getLogger(GroupController.class);


	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public Iterable<?> findAll() {
		logger.info(">>>>>> Find All Groups");
		
		List<Group> groups = new ArrayList<Group>();
		Group g1 = new Group();
		g1.setName("Group1");
		groups.add(g1);
		
		Group g2 = new Group();
		g2.setName("Group1");
		groups.add(g2);
		
		return groups;
	}
}
