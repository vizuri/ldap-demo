package com.vizuri.demo;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.naming.Name;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.DirContextAuthenticationStrategy;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.support.LdapNameBuilder;



@Configuration
public class LdapConfig {
	
	private static Logger logger = LoggerFactory.getLogger(LdapConfig.class);



	@Value("${ldap.url}")
	private String ldapUrl;
	@Value("${ldap.partitionSuffix}")
	private String ldapPartitionSuffix;
	@Value("${ldap.principal}")
	private String ldapPrincipal;
	@Value("${ldap.password}")
	private String ldapPassword;
	
	
	@Bean 
	@Autowired
	public LdapContextSource contextSource() {
	    LdapContextSource contextSource = new LdapContextSource();
	    logger.info(">>> ldapUrl:" + ldapUrl) ;
	    logger.info(">>> ldapPartitionSuffix:" + ldapPartitionSuffix) ;
	    logger.info(">>> ldapPrincipal:" + ldapPrincipal) ;
	    logger.info(">>> ldapPassword:" + ldapPassword) ;
	    
	    contextSource.setUrl(ldapUrl);
	    contextSource.setBase(ldapPartitionSuffix);
	    contextSource.setUserDn(ldapPrincipal);
	    contextSource.setPassword(ldapPassword);
	    
	
	    
	    return contextSource;
	}
	
	@Bean
	@Autowired
	public LdapTemplate ldapTemplate(LdapContextSource contextSource) throws Exception {
		logger.info("getLdapTemplate:" + contextSource);		
		LdapTemplate ldapTemplate = new LdapTemplate(contextSource);
	    return ldapTemplate;
	}
	
	
	


}
