package com.vizuri.demo;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.SearchControls;

import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.DistinguishedName;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.filter.Filter;



public class TestLDAPConnection {
	public static void main(String args[]) throws Exception {
		LdapContextSource contextSource = new LdapContextSource();
		contextSource.setUrl("ldap://localhost:389");
		contextSource.setBase("DC=VIZURI,DC=COM");
		contextSource.setUserDn("cn=Directory Manager");
		contextSource.setPassword("P@ssw0rd");
		contextSource.afterPropertiesSet();

		LdapTemplate ldapTemplate = new LdapTemplate(contextSource);
		ldapTemplate.afterPropertiesSet();

		// Perform the authentication.
		Filter filter = new EqualsFilter("uid", "admin");

		boolean authed = ldapTemplate.authenticate("OU=People", filter.encode(), "P@ssw0rd");

		// Display the results.
		System.out.println("Authenticated: " + authed);
		
		SearchControls controls = new SearchControls();
		controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		
		Attribute objectClass = new BasicAttribute("objectClass");
		{
			objectClass.add("top");
			objectClass.add("uidObject");
			objectClass.add("person");
			objectClass.add("organizationalPerson");
		}
		Attributes userAttributes = new BasicAttributes();
		userAttributes.put(objectClass);
		userAttributes.put("cn", "Kent");
		userAttributes.put("sn", "Eudy");
		userAttributes.put("uid", "keudy");
		// userAttributes.put("postalAddress", user.getPostalAddress());
		// userAttributes.put("telephoneNumber", user.getTelephoneNumber());
		userAttributes.put("userPassword", "P@ssw0rd".getBytes());
		//ldapTemplate.bind(bindDN("keudy"), null, userAttributes);
	
		
		List<User> list = ldapTemplate.search(DistinguishedName.EMPTY_PATH, "(objectclass=person)", controls,
				new UserAttributesMapper());
		
		System.out.println("Users:" + list.size());
		
		// Perform the authentication.
		Filter filter2 = new EqualsFilter("uid", "keudy");

		boolean authed2 = ldapTemplate.authenticate("OU=People", filter2.encode(), "P@ssw0rd");
		
		System.out.println("Authenticated2:" + authed2);


	}
	public static javax.naming.Name bindDN(String _x) {
		@SuppressWarnings("deprecation")
		javax.naming.Name name = new DistinguishedName("uid=" + _x + ",ou=People");
		return name;
	}

}
 class UserAttributesMapper implements AttributesMapper<User> {

	@Override
	public User mapFromAttributes(Attributes attributes) throws NamingException {
		User user;
		if (attributes == null) {
			return null;
		}
		user = new User();
		user.setFirstName(attributes.get("cn").get().toString());

		if (attributes.get("userPassword") != null) {
			String userPassword = null;
			try {
				userPassword = new String((byte[]) attributes.get("userPassword").get(), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				//logger.error("unable to process", e);
			}
			user.setPassword(userPassword);
		}
		if (attributes.get("uid") != null) {
			user.setUsername(attributes.get("uid").get().toString());
		}
		if (attributes.get("sn") != null) {
			user.setLastName(attributes.get("sn").get().toString());
		}
		// if (attributes.get("postalAddress") != null) {
		// user.setPostalAddress(attributes.get("postalAddress").get().toString());
		// }
		// if (attributes.get("telephoneNumber") != null) {
		// user.setTelephoneNumber(attributes.get("telephoneNumber").get().toString());
		// }
		return user;
	}
}